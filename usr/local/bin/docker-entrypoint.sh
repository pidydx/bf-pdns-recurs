#!/bin/bash

set -e

if [ "$1" = 'pdns_recursor' ]; then
    exec /usr/local/sbin/pdns_recursor
fi

exec "$@"