#########################
# Create base container #
#########################
FROM ubuntu:24.04 as base
LABEL maintainer="pidydx"

# Base setup
ENV APP_USER=pdns
ENV APP_GROUP=pdns

ENV PDNS_RECURSOR_VERSION=5.1.0

ENV BASE_PKGS libboost-context1.83.0 libluajit-5.1-2 lua5.4

# Update users and groups
RUN userdel ubuntu
RUN groupadd -g 1000 ${APP_GROUP} \
 && useradd -M -N -u 1000 ${APP_USER} -g ${APP_GROUP} -s /usr/sbin/nologin

# Update and install base packages
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ca-certificates \
 && apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_PKGS} \
 && rm -rf /var/lib/apt/lists/*


##########################
# Create build container #
##########################
FROM base AS builder

# Set build dependencies
ENV BUILD_DEPS autoconf automake build-essential cargo curl git libboost-context-dev libboost-dev libboost-serialization-dev \
               libboost-system-dev libboost-thread-dev libluajit-5.1-dev libssl-dev libtool pkg-config ragel wget

RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BUILD_DEPS}

# Run build
WORKDIR /usr/src
RUN wget -nv https://downloads.powerdns.com/releases/pdns-recursor-${PDNS_RECURSOR_VERSION}.tar.bz2
RUN tar xf pdns-recursor-${PDNS_RECURSOR_VERSION}.tar.bz2
WORKDIR /usr/src/pdns-recursor-${PDNS_RECURSOR_VERSION}
RUN ./configure --sysconfdir=/etc/pdns --mandir=/usr/share/man
RUN make
RUN make install-strip


##########################
# Create final container #
##########################
FROM base

# Prepare container
COPY --from=builder /usr/local /usr/local/
COPY usr/ /usr/

RUN mkdir -p /run/pdns-recursor \
 && chown ${APP_USER}:${APP_GROUP} /run/pdns-recursor

EXPOSE 5353/tcp 5353/udp
VOLUME ["/etc/pdns"]

USER $APP_USER
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["pdns_recursor"]
